defmodule TwoToOne do
  def longest(a, b), do: String.split(a, "") ++ String.split(b, "") |> Enum.uniq() |> Enum.sort() |> Enum.join()
end